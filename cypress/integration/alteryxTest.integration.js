
const BASE_URL = Cypress.config().baseUrl;
const FIRST_URL = `${BASE_URL}/first`;
const SECOND_URL = `${BASE_URL}/second`;

const RED_TEXT = 'Incorrect';
const GREEN_TEXT = 'Correct';

const RED_COLOR = 'rgb(255, 0, 0)';
const GREEN_COLOR = 'rgb(0, 255, 0)';


describe('Alteryx automation task', () => {
  before(() => {
    cy.visit('/');
  });

  it('Test', () => {
    // check if url is right
    cy.url().should('eq', FIRST_URL);

    // click on button and check if url is right after redirect
    cy.get('[try-to-find-this-attribute="find-by-me"]').click();
    cy.url().should('eq', SECOND_URL);

    // check that h2 text is 'Incorrect' and color is red
    cy.get('h2').should('be.visible');
    cy.get('h2').invoke('text').should('eq', RED_TEXT);
    cy.get('h2').invoke('css', 'color').should('eq', RED_COLOR);

    //type correct value and check that h2 text is 'Correct' and color is green
    cy.get('span').should('be.visible');
    cy.get('span').invoke('text').then(spanText => {
      cy.get('input').type(spanText);
      cy.get('h2').invoke('text').should('eq', GREEN_TEXT);
      cy.get('h2').invoke('css', 'color').should('eq', GREEN_COLOR);
    });

    //type incorrect value and check that h2 text is 'Correct' and color is green
    cy.get('input').clear();
    cy.get('input').type(`Incorrect text ${new Date().getTime()}`);
    cy.get('h2').invoke('text').should('eq', RED_TEXT);
    cy.get('h2').invoke('css', 'color').should('eq', RED_COLOR);
  });

  after(() => {
    cy.get('input').clear();
  });
});